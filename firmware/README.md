# Badge development

To develop code on the badge, you need to set up a development environment (using ESP IDF for the compiling and linking), and you need to make sure that when code is compiled you can flash the badge.

## 0. Prerequisites
The following instructions are based on the assumption you are using a Linux system.

Known issues:
- WSL (Windows Subsystem for Linux) will not work, since it is unable to use the USB device.
- MacOS can't easily map USB inside the container.

All these issues can be worked around by installing ESP IDF plugin on VScode and choosing v5.1.2. It's also faster to set up.

## 1. Connecting your device

Depending on your distro

```shell
$ sudo tail -f /var/log/syslog | grep tty
```

or

```shell
$ sudo dmesg -w | grep tty
```

Switch your badge on, then connect it to your machine.

Terminal should show some lines giving you a hint at which unix device you will be able to reach your badge. Typically, this would be something like `/dev/ttyACM0`, use it instead of `[PORT]` in the following istructions.

## 2. Checking out the repo

```shell
$ git clone https://gitlab.com/tidklaas/hip-badge
$ git submodule update --init --recursive
$ cd hip-badge/firmware/blinkenlights
```

## 3. Using ESP IDF natively or containerized

### ESP IDF in a container

```shell
sudo docker run -i -t -v $PWD:/project -w /project --device [/dev/PORT] espressif/idf:v5.1.2
```

### ESP IDF Local installation

Development is done on the v5.1.x branch of the ESP-IDF. If you have not
installed the ESP-IDF, please follow the excellent instructions at
https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32c3/get-started/index.html

If you already have an installed ESP IDF, check out the latest v5.1.x tag:

```shell
$ cd $IDF_PATH
$ git fetch
$ git checkout v5.1.2
$ git submodule update --init --recursive
$ bash install.sh
```
### ESP IDF VSCode plugin installation

Start from step 3 https://github.com/espressif/vscode-esp-idf-extension/blob/master/docs/tutorial/install.md

Select open folder and open blinkenlighs subdirectory of repo
At bottom of screen in status line select connected device it will be named something like `/dev/cu.usbmodem14011` depends on OS it will take some time for a pop up in searchbox to appear, wait for it and select device.

Next choose device target click on esp32 near device select at bottom of screen, wait a bit and select esp32c3

That should do the trick, run menuconfig a cogwheel icon to create initial configuration

Fire icon - build, flash, monitor 

## 4. Running/modifying the firmware

To get started with your HiP Badge on a computer with a ESP IDF installation:

```shell
# proceeding from p2, your workdir is firmware/blinkenlights
$ idf.py set-target esp32c3
$ idf.py menuconfig # --help
# select and enter: Blinkenlights Configuration  --->
```

## 5. Updating Blinkenlights config

If buttons do not work according to [documentation](documents/README.md), depending on your badge version:

### 1 gen
(That's distributed on the HiP conference)

### 2 and 3 gen

Buttons assignment
9 -> 8
10 -> 9

Hit `S` to save, then build and re-flash as follows.

## 6. (Optional) Re-flashing firmware

You should now be set up for development. The main file with blinkenlight is located in `main/hipbadge.c`. Make the changes you need, then

```shell
$ idf.py build
```

and if that threw no errors, connect your device and do

```shell
$ idf.py -p [/dev/PORT] flash
```

If this did not work, you can try to do

```shell
$ idf.py -p [/dev/PORT] erase-flash
$ idf.py -p [/dev/PORT] erase-otadata
$ idf.py -p [/dev/PORT] flash
```

Right after flashing, your device should reboot and operate with your changes.

### Batch programming

Consider using four terminal windows and endless waiting loop conditions:

```shell
while : ; do [ -e /dev/ttyACM0 ] && esptool.py -p /dev/ttyACM0 --baud 921600 --chip esp32c3 write_flash 0x0 bootloader/bootloader.bin 0x8000 partition_table/partition-table.bin 0x10000 blinkenlights.bin && while [ -e /dev/ttyACM0 ] ; do : ; done; done
```

Other useful commands when batch programming in serial production are:

```shell
$ esptool.py -p [/dev/PORT] --baud 921600 or 460800 --chip esp32c3 --before default_reset --after hard_reset write_flash 0x0 /path/to/esp-file.bin
$ esptool.py -p [/dev/PORT] --baud 921600 --chip esp32c3 verify_flash 0x10000 /path/to/esp-file.bin
$ esptool.py -p [/dev/PORT] read_mac >store-the-mac-.txt
$ esptool.py -p [/dev/PORT] chip_id >store-the-chipid-.txt
$ esptool.py -p [/dev/PORT] flash_id >store-the-flashid-.txt
```
